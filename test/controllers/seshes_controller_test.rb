require 'test_helper'

class SeshesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sesh = seshes(:one)
  end

  test "should get index" do
    get seshes_url, as: :json
    assert_response :success
  end

  test "should create sesh" do
    assert_difference('Sesh.count') do
      post seshes_url, params: { sesh: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show sesh" do
    get sesh_url(@sesh), as: :json
    assert_response :success
  end

  test "should update sesh" do
    patch sesh_url(@sesh), params: { sesh: {  } }, as: :json
    assert_response 200
  end

  test "should destroy sesh" do
    assert_difference('Sesh.count', -1) do
      delete sesh_url(@sesh), as: :json
    end

    assert_response 204
  end
end
