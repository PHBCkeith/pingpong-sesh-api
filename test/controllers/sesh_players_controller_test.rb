require 'test_helper'

class SeshPlayersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sesh_player = sesh_players(:one)
  end

  test "should get index" do
    get sesh_players_url, as: :json
    assert_response :success
  end

  test "should create sesh_player" do
    assert_difference('SeshPlayer.count') do
      post sesh_players_url, params: { sesh_player: { losses: @sesh_player.losses, name: @sesh_player.name, sesh_id: @sesh_player.sesh_id, wins: @sesh_player.wins } }, as: :json
    end

    assert_response 201
  end

  test "should show sesh_player" do
    get sesh_player_url(@sesh_player), as: :json
    assert_response :success
  end

  test "should update sesh_player" do
    patch sesh_player_url(@sesh_player), params: { sesh_player: { losses: @sesh_player.losses, name: @sesh_player.name, sesh_id: @sesh_player.sesh_id, wins: @sesh_player.wins } }, as: :json
    assert_response 200
  end

  test "should destroy sesh_player" do
    assert_difference('SeshPlayer.count', -1) do
      delete sesh_player_url(@sesh_player), as: :json
    end

    assert_response 204
  end
end
