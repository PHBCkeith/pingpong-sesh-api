class Api::V1::PlayersController < ApplicationController
  before_action :set_player, only: [:show, :update, :destroy]

  # GET api/v1/players
  def index
    @players = Player.all

    render json: @players
  end

  # GET api/v1/players/1
  def show
    if @player && !@player.sesh_players.empty?
      @player = SeshPlayer.includes(:player).where(player_id: params[:id])
        .select("player_id, sum(wins) as sum_wins, sum(losses) as sum_losses").group(:player_id).map { |sesh_player| 
          { id: sesh_player.player.id, name: sesh_player.player.name, wins: sesh_player.sum_wins, 
            losses: sesh_player.sum_losses } 
          }.first
    else
      @player = { id: @player.id, name: @player.name, wins: 0, losses: 0 }
    end
    render json: @player
  end

  # POST api/v1/players
  def create
    @player = Player.new(player_params)

    if @player.save
      render json: @player, status: :created
    else
      render json: @player.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT api/v1/players/1
  def update
    if @player.update(player_params)
      render json: @player
    else
      render json: @player.errors, status: :unprocessable_entity
    end
  end

  # DELETE api/v1/players/1
  def destroy
    @player.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_player
      @player = Player.find(params[:id])
    end

    def set_player_with_record
      @player = set_player.include(:sesh_players).group()
    end

    # Only allow a trusted parameter "white list" through.
    def player_params
      params.require(:player).permit(:name)
    end
end
