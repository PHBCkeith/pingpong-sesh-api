class Api::V1::SeshesController < ApplicationController
  before_action :set_sesh, only: [:update, :destroy]
  before_action :get_full_sesh, only: [:show]

  # GET /seshes
  def index
    @seshes = getSeshes 

    render json: @seshes
  end

  # GET /seshes/1
  def show
    # @sesh = getSesh(params[:id])
    render json: @sesh
  end

  # POST /seshes
  def create
    @sesh = Sesh.new(sesh_params)

    if @sesh.save
      render json: @sesh, status: :created, location: api_v1_sesh_url(@sesh)
    else
      render json: @sesh.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /seshes/1
  def update
    if @sesh.update(sesh_params)
      render json: @sesh
    else
      render json: @sesh.errors, status: :unprocessable_entity
    end
  end

  # DELETE /seshes/1
  def destroy
    @sesh.destroy
  end

  private
    # returns array of sesh info with player data merged with win and loss data
    def getSeshes
      Sesh.includes(:players, :sesh_players).map { |sesh|
        { id: sesh.id, players: sesh.sesh_players.map { |sesh_player| 
          { id: sesh_player.player.id, name: sesh_player.player.name,
            wins: sesh_player.wins, losses: sesh_player.losses}  
        }}
      }
    end

    # returns object containing info for sesh with given id including player data merged with win and loss data
    def get_full_sesh
      @sesh = {id: params[:id], players: set_sesh.sesh_players.includes(:player)
                .order(wins: :asc, losses: :desc).map { |sesh_player| 
                  { id: sesh_player.player.id, name: sesh_player.player.name, 
                    wins: sesh_player.wins, losses: sesh_player.losses } }
                }     
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_sesh
      @sesh = Sesh.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def sesh_params
      params.fetch(:sesh, {})
    end
end
