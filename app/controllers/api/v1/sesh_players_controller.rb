class Api::V1::SeshPlayersController < ApplicationController
  before_action :set_sesh_player, only: [:show]
  before_action :set_sesh, except: [:update, :destroy]
  before_action :set_sesh_player_by_player_id, only: [:update, :destroy]

  # GET api/v1/sesh/:sesh_id/sesh_players
  def index
    @sesh_players = @sesh.sesh_players

    render json: @sesh_players
  end

  # GET /sesh_players/1
  def show
    render json: @sesh_player
  end

  # POST api/v1/seshes/:sesh_id/sesh_players
  def create
    player_name = params[:sesh_player][:name]
    if @sesh
      if !@player = Player.find_by(name: player_name)
        @player = @sesh.players.create!(name: player_name)
        render json: @player
      elsif !@sesh.players.find_by(name: player_name)
        @sesh.players << @player
        render json: @player
      end
    end
    # if @sesh && @sesh_player = @sesh.sesh_players.create!(sesh_player_params)
    #   if !@sesh_player.player
    #     @sesh_player.player.create!()
    #   end
    #   render json: @sesh_player, status: :created#, location: api_v1_sesh_players_url(@sesh_player.id)
    # else
    #   render json: @sesh_player.errors, status: :unprocessable_entity
    # end
  end

  # PATCH/PUT /sesh_players/1
  def update
    if @sesh_player.update(sesh_player_params)
      render json: @sesh_player
    else
      render json: @sesh_player.errors, status: :unprocessable_entity
    end
  end

  # DELETE /sesh_players/1
  def destroy
    @sesh_player.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sesh_player
      @sesh_player = SeshPlayer.find(params[:id])
    end

    def set_sesh_player_by_player_id
      @sesh_player = set_sesh.sesh_players.where(player_id: params[:id]).first
    end

    # Only allow a trusted parameter "white list" through.
    def sesh_player_params
      params.require(:sesh_player).permit(:sesh_id, :wins, :losses)
    end

    def set_sesh
      @sesh = Sesh.find(params[:sesh_id])
    end
end
