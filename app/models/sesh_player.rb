class SeshPlayer < ApplicationRecord
  belongs_to :sesh
  belongs_to :player
  before_save :default_values

  def default_values
    self.wins = 0 if self.wins.nil?
    self.losses = 0 if self.losses.nil?
  end
end
