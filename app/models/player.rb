class Player < ApplicationRecord
  has_many :sesh_players, -> { order(wins: :desc, losses: :asc) }
  has_many :seshes, through: :sesh_players
  validates :name, presence: true
  validates :name, uniqueness: { case_sensitive: true }

end
