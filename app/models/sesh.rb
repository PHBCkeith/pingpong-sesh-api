class Sesh < ApplicationRecord
  has_many :sesh_players, -> { order(wins: :desc, losses: :asc) }
  has_many :players, through: :sesh_players
end
