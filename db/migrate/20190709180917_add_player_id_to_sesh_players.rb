class AddPlayerIdToSeshPlayers < ActiveRecord::Migration[5.2]
  def change
    add_column :sesh_players, :player_id, :integer
  end
end
