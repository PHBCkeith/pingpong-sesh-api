class Change < ActiveRecord::Migration[5.2]
  def change
    change_column_null :sesh_players, :sesh_id, false
    change_column_null :sesh_players, :player_id, false
  end
end
