class CreateSeshPlayers < ActiveRecord::Migration[5.2]
  def change
    create_table :sesh_players do |t|
      t.string :name
      t.integer :sesh_id
      t.integer :wins
      t.integer :losses

      t.timestamps
    end
  end
end
