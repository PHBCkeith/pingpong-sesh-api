class AddIndexToSeshPlayers < ActiveRecord::Migration[5.2]
  def change
    add_index :sesh_players, [:sesh_id, :player_id], unique: true, name: :index_unique_field_combination
  end
end
