class RemoveNameFromSeshPlayers < ActiveRecord::Migration[5.2]
  def change
    remove_column :sesh_players, :name
  end
end
